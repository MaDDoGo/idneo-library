'use strict';

angular.module('myApp.bookList', ['ngRoute'])

  .config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/book-list', {
      templateUrl: 'book-list/book-list.html',
      controller: 'bookListCtrl'
    });
  }])

  .controller('bookListCtrl', ['$scope', '$uibModal', function ($scope, $uibModal) {
    $scope.books = [];

    $scope.books = [{
      title: 'The Lord of The Rings',
      author: 'J. R. R. Tolkien',
      thumbnailUrl: 'https://luccomm400.files.wordpress.com/2014/10/lord_of_the_rings_book_cover_by_mrstingyjr-d5vwgct.jpg'
    }, {
      title: 'Game of Thrones 1',
      author: 'J. R. R. Martin',
      thumbnailUrl: 'http://i.imgur.com/IibDqjf.jpg'
    }, {
      title: 'Game of Thrones 2',
      author: 'J. R. R. Martin',
      thumbnailUrl: 'http://i.imgur.com/IibDqjf.jpg'
    }, {
      title: 'Game of Thrones 3',
      author: 'J. R. R. Martin',
      thumbnailUrl: 'http://i.imgur.com/IibDqjf.jpg'
    }, {
      title: 'Game of Thrones 4',
      author: 'J. R. R. Martin',
      thumbnailUrl: 'http://i.imgur.com/IibDqjf.jpg'
    }, {
      title: 'Game of Thrones 4',
      author: 'J. R. R. Martin',
      thumbnailUrl: 'http://i.imgur.com/IibDqjf.jpg'
    }, {
      title: 'Game of Thrones 5',
      author: 'J. R. R. Martin',
      thumbnailUrl: 'http://i.imgur.com/IibDqjf.jpg'
    }];

    $scope.edit = function (book, parentSelector) {
      var parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'myModalContent.html',
        controller: 'ModalInstanceCtrl',
        controllerAs: '$ctrl',
        size: 'lg',
        appendTo: parentElem,
        resolve: {
          book: function () {
            return book;
          }
        }
      });
    };
  }]);
