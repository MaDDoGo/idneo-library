'use strict';

angular.module('myApp.bookSearch', ['ngRoute'])

  .config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/book-search', {
      templateUrl: 'book-search/book-search.html',
      controller: 'bookSearchCtrl'
    });
  }])

  .controller('bookSearchCtrl', ['$scope', function ($scope) {
    function resetFilters() {
      $scope.filter = {
        type: {
          and: [],
          or: []
        },
        category: {
          and: [],
          or: []
        }
      };
      $scope.type = '';
      $scope.category = '';

      $scope.typeAnd = true;
      $scope.categoryAnd = true;
    }

$scope.books = [{
    title: 'The Lord of The Rings',
    author: 'J. R. R. Tolkien',
    thumbnailUrl: 'https://luccomm400.files.wordpress.com/2014/10/lord_of_the_rings_book_cover_by_mrstingyjr-d5vwgct.jpg'
  }, {
    title: 'Game of Thrones 1',
    author: 'J. R. R. Martin',
    thumbnailUrl: 'http://i.imgur.com/IibDqjf.jpg'
  }, {
    title: 'Game of Thrones 2',
    author: 'J. R. R. Martin',
    thumbnailUrl: 'http://i.imgur.com/IibDqjf.jpg'
  }, {
    title: 'Game of Thrones 3',
    author: 'J. R. R. Martin',
    thumbnailUrl: 'http://i.imgur.com/IibDqjf.jpg'
  }, {
    title: 'Game of Thrones 4',
    author: 'J. R. R. Martin',
    thumbnailUrl: 'http://i.imgur.com/IibDqjf.jpg'
  }, {
    title: 'Game of Thrones 4',
    author: 'J. R. R. Martin',
    thumbnailUrl: 'http://i.imgur.com/IibDqjf.jpg'
  }, {
    title: 'Game of Thrones 5',
    author: 'J. R. R. Martin',
    thumbnailUrl: 'http://i.imgur.com/IibDqjf.jpg'
  }];

    $scope.typeBlur = function ($event) {
      if ($scope.type === '') return;

      pushValue('type', $scope.typeAnd ? 'and' : 'or', $scope.type);
      $scope.type = '';
      //angular.element(document.querySelector('#type')).focus();
    };

    $scope.categoryBlur = function ($event) {
      if ($scope.category === '') return;

      pushValue('category', $scope.categoryAnd ? 'and' : 'or', $scope.category);
      $scope.category = '';
    };

    $scope.checkType = function ($event) {
      if ($scope.type.length > 0 && $event.key === ',') {
        pushValue('type', $scope.typeAnd ? 'and' : 'or', $scope.type.slice(0, -1));
        $scope.type = '';
      }
    };

    $scope.checkCategory = function ($event) {
      console.log($event.key);
      if ($scope.category.length > 0 && $event.key === ',') {
        pushValue('category', $scope.categoryAnd ? 'and' : 'or', $scope.category.slice(0, -1));
        $scope.category = '';
      }
    };

    function pushValue(type, boolType, value) {
      if($scope.filter[type][boolType].indexOf(value) === -1)
        $scope.filter[type][boolType].push(value);
    }

    resetFilters();
  }]);