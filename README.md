# Questions #

## 1. Design a basic User Interface of the Web Page to display and manage available books of the digital library. ##

The UI design is in the repository. It is an Angular SPA with two views (List and Search books).

## 2. Create Database structure to organize the digital library. ##

**Books**:

* Id
* Title
* Author
* ReleaseDate
* Genres (M-N Genres)
* Type (N-1 Types)
* CreatedBy
* CreatedDate
* ModifiedBy
* ModifiedDate

**Authors**:

* Id
* Name
* BirthDate

**Genres**:

* Id
* Name

**Types**:

* Id
* Name
* Extension
* MIME Type

**Users**:

* Id
* Name
* Username
* CreatedDate
* ModifiedDate
* Books: List of Books that has the User

## 3. In order to search a book using web engine, is needed to design an algorithm in the backend of the web page to find the most suitable book for the user. Basically, the algorithm should take into account user filters (type, category) with logical conditions for each ones. Imagine you have this user interface: ##

### a) Create the algorithm of the backend process when search button is pressed ###

1. POST request received
2. Backend authorizing
3. Backend routing
4. Tamper data
5. Business call to search with received filters
6. Data layer call to search with received filters
7. Data received from the data storage
8. Data serialization to send with the response
9. Headers set and information send

### b) Which is the most suitable data structure to display the result of the search? Why? Give an example with dummy data. ###

I think the most suitable data structure is a list in a format like JSON. JSON allows us to serialize the data and treat it faster and lighter than another format like XML.

Example:
 
```
[{
    id: 1,
    title: 'The Lord of the Rings',
    author: {
        id: 1,
        name: 'J.R.R.Tokien'
    },
    genres: [{
        id: 1,
        name: 'Fantastic'
    }],
    type: {
        id: 1,
        name: 'ePub'
    },
    createdBy: {
        id: 1
        name: 'Enric'
    },
    createdDate: 2016-11-23 22:00
}, {
    id: 2,
    title: 'Pillars of Earth',
    author: {
        id: 2,
        name: 'Ken Follet'
    },
    genres: [{
        id: 1,
        name: 'Fantastic'
    }, {
        id: 2,
        name: 'Historical'
    }],
    type: {
        id: 1,
        name: 'ePub'
    },
    createdBy: {
        id: 1
        name: 'Enric'
    },
    createdDate: 2016-11-23 22:01
}...]
```

## 4. Propose a technology to develop this computer system. Why? ##

I propose NodeJS to develop this computer system. One of the main reasons of the choice is the ability to do code that is reusable in front and backend. It has so many developers working on it and there are lots of libraries that simplifies most of the hard work.

Another reason is the ligthness of the system: nodeJS is a very lightweight server and a very scalable system. That means that less resources are needed to use it and that it is possible to scale the system if the traffic is very intense in a very easy way

## 5. Propose a class diagram that fits with this computer system. ##

![Class.png](https://bitbucket.org/repo/X4KMob/images/1813478043-Class.png)
